package edu.umb.workshop.chatClient.Models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class ChatModel implements Runnable
{

    private ArrayList<String> messages;
    private static Socket socket;
    private BufferedReader reader;
    private PrintWriter writer;

    public void init() {
        messages = new ArrayList<>();
        try {
            socket = new Socket("localhost", 8888);
            reader = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    public void sendMessage(String mensaje) {
        writer.println(mensaje);
    }
    
    public ArrayList<String> getMessages() {
        return messages;
    }

    @Override
    public void run() {
        while (socket.isConnected()) {
            try {
                String response = reader.readLine();
                messages.add(response);
            } catch (IOException ex) {
                System.out.println("Error: " + ex.getMessage());
            }
        }
    }
}
