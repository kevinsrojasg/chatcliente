package edu.umb.workshop.chatClient.Controlllers;

import edu.umb.workshop.chatClient.Views.MainWindow;
import edu.umb.workshop.chatClient.Models.ChatModel;

public class ChatController implements Runnable
{

    private final ChatModel model;
    private final MainWindow chatWindow;

    public ChatController() {
        model = new ChatModel();
        chatWindow = new MainWindow(this);
    }

    public void init() {
        chatWindow.setVisible(true);
        model.init();
        getMessages();
    }

    public void sendMessage(String mensaje) {
        model.sendMessage(mensaje);
    }

    public void getMessages() {
        Thread hiloModelo = new Thread(model);
        hiloModelo.start();
        Thread controllerThread = new Thread(this);
        controllerThread.start();
    }

    @Override
    public void run() {

        while (true) {
            StringBuilder mensajes = new StringBuilder();
            model.getMessages().forEach((mensaje) -> {
                mensajes.append(mensaje).append("\n");
            });
            chatWindow.getChatTextArea().setText(mensajes.toString());
        }
    }
}
